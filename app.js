var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
//  Se elimina la librería para manejar la sesión de usuario.
var session = require('express-session');

var login = require('./routes/login');
var users = require('./routes/users');
var mesas = require('./routes/mesas');
var tiempo = require('./routes/tiempo');
var menu = require('./routes/menu');
var atencion = require('./routes/atencion');
var pagar = require('./routes/pagar');

//  Se carga archivo de configuración.
global.configuracion = require('./config.json');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

//  Se agrega configuración para el control de sesión.
app.use(session({ secret : global.configuracion.session.secret, cookie : { maxAge : 3600000 } , resave : true, saveUninitialized : true }));

app.use('/', login);
app.use('/users', users);
app.use('/mesas', mesas);
app.use('/tiempo', tiempo);
app.use('/menu', menu);
app.use('/atencion', atencion);
app.use('/pagar', pagar);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
