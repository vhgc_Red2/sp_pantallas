/*******************************************************************************
 *  Archivo javascript que se encarga de crear el mensaje de alerta que se va a 
 *  pintar y se encargará de gestionar los errores de la aplicación.
 ******************************************************************************/
var mensajeAlerta='<div class="container fondo_cargando" id="msjAlerta"><div class="col-12"><div class="row middle center tamVentana"><div class="fondo_msjAlertaMsj"><div class="posicionTitulo"><p id="p_titulo_msjAlerta" class="titulo_msjAlerta">Titulo</p><img id="img_cerrar_msjAlerta" class="img_salir_msjAlerta" src="/images/exit_button.svg"></div><div class="tamano_ventanaAlerta"><p style="padding: 1%;" id="p_text_msjAlerta"></p></div><div class="posicionbtn_msjAlerta"><button id="btn_msjAlerta" class="btn_In"></button></div></div></div></div></div>';

$(document).ready(function () { });

//  Función que se encarga de agregar el mensaje de alerta a la página donde sea llamado
function mensajeAlertaAdd(){
    //  Se carga el mensaje alerta al body
    $('body').append(mensajeAlerta);
    //  Se agregan eventos click al botón de mensajeAlerta y imagen "x"
    $('#btn_msjAlerta, #img_cerrar_msjAlerta').click(function(){
        //  Se le agrega función que se inicializará cuando se detecte
        //  el click en alguno de los elementos declarados
        eventBotton($(this))
    });
}

//  Función que se encargará de mostrar el mensaje de alerta con los
//  parámetros que reciba (De donde sea instanciada la función).
function cargarmensajeAlerta(titulo, mensajeError, textoBoton){
    //  Se muestra el mensaje alerta
    $('#msjAlerta').show();
    //  Se le agrega el titulo al mensaje
    $('#p_titulo_msjAlerta').text('Error '+ titulo);
    //  Se agrega el mensaje de error al mensaje
    $('#p_text_msjAlerta').text(mensajeError);
    //  Se cambia el texto que contiene el botón de acuerdo a la acción
    //  Seleccionada.
    $('#btn_msjAlerta').html(textoBoton);
    //  Se carga al elemento HTML el atributo acción, con la acción que requiere que realice el mensaje
    $('#btn_msjAlerta, #img_cerrar_msjAlerta').attr('accion', textoBoton);
}

//  Función que se ejecuta una vez que es detectado el click
//  en alguno de los elementos previamente descritos en la función 
function eventBotton(elemento){
    //  Se toma el valor del atributo acción
    var accionBoton = $(elemento).attr('accion');

    //  En base a la acción que se tenga en el botón, se realizarán ciertas acciones
    switch (accionBoton) {
        case 'Recargar' : {
            //  En caso de que la opción sea 'Recargar', se deberá de recargar
            //  la pantalla dando la pauta para que se pueda volver a realizar 
            //  la petición con el microservicio.
            location.reload();
        }
        break;

        case 'Cerrar' : {
            //  En caso de que sea 'Cerrar', únicamente se ocultará el mensaje de
            //  alerta de la aplicación
            quitarmensajeAlerta();
        }
        break;
        
        case 'Salir' : {
            //  Con la opción salir, se intentará cerrar la sesión de usuario.
            $.ajax({
                type : 'POST',
                url : '/salir',
                dataType : 'json',
                success : function(res, status, xhr){
                    //  Si la petición de cierre de sesión fue exitosa, la aplicación se redirecciona a la pantalla de inicio
                    window.location.href =  res.pantalla;
                },
                error : function(xhr, status, error){
                    console.log('Error al cerrar la sesión....?');
                }
                
            }); 
        }
        break;
    }
}

//  Función que se encarga de ocultar el mensaje de alerta.
function quitarmensajeAlerta(){
    $('#msjAlerta').hide();
}