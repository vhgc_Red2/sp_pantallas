$(document).ready(function(){
    //  Se muestra en la parte del header que contiene el nombre y el apellido del usuario
    $('#identificacionUsuario').show();
    
    //  Se identifica cuando se le da click a la clase "atencionOrdenarMas", y se redirecciona a la pantalla de "menu"
    $('.atencionOrdenarMas').click(function(){
        window.location.href =  'menu';
    });
    
    //  Se identifica cuando se le da click a la clase "atencionPagar", y se redirecciona a la pantalla de "pagar"
    $('.atencionPagar').click(function(){
        window.location.href =  'pagar';
    });

});