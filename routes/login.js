var express = require('express');
var router = express.Router();
var request = require('request');

//  Variable que gestionará la sesion de usuario;
var sesion;

//  Función que retorna la fecha del sistema
function fecha() {

    var d = new Date();
    var dia = d.getDate();
    var arrMeses = new Array();
    arrMeses[0] = "Enero";
    arrMeses[1] = "Febrero";
    arrMeses[2] = "Marzo";
    arrMeses[3] = "Abril";
    arrMeses[4] = "Mayo";
    arrMeses[5] = "Junio";
    arrMeses[6] = "Julio";
    arrMeses[7] = "Agosto";
    arrMeses[8] = "Septiembre";
    arrMeses[9] = "Octubre";
    arrMeses[10] = "Noviembre";
    arrMeses[11] = "Diciembre";
    var mes = arrMeses[d.getMonth()];
    var ano = d.getFullYear();
    var fechaActual = dia + ' de ' + mes + ' de ' + ano;
    return fechaActual;
}

/* Login al sistema*/
router.get('/', function (req, res, next) {
    sesion = req.session;
    if (sesion.token){
        //   Si el token existe en la sesión de usuario, deberá de mandar a la pantalla según su usuario
        var pantalla = validaDireccionamiento(sesion.rol);
        res.redirect(pantalla);

    } else {
        //  Si no, pinta la pantalla de login, para que se haga el proceso.
        res.render('login', {
            title: 'Bienvenido, inicia sesión',
            name : ' ',
            lastname : ' ',
            fecha : fecha()});
    }

});

/* Petición POST a microservicio de autenticación, para validar si el usuario puede o no acceder al sistema */
router.post('/login', function (req, resp){
    //  Se captura la sesión de usuario.
    sesion = req.session;
    if (sesion.token){
        //  No permite que se realice la petición. Se debe redireccionar a la pantalla.
        var pantalla = validaDireccionamiento(sesion.rol);
        res.redirect(pantalla);

    } else {
        //  console.log(req.body);

        //  Se establece el esqueleto de la petición.
        var datosPeticion = {
            url : 'http://' + global.configuracion.servidorAutenticacion.direccionIP + ':' + global.configuracion.servidorAutenticacion.puerto + global.configuracion.servidorAutenticacion.listaEndpoints.login,
            method : 'POST',
            json : true,
            body : req.body
        }

        //  Se realiza la petición a microservicio.
        request(datosPeticion, function(err, response, body){

            //  Si la petición no se pudo realizar...
            if(err){
                console.error('Hubo error al realizar la petición de inicio de sesión.');
                //  En caso de que haya existido un error en la petición, se informa a usuario
                resp.status(global.configuracion.estatusError.notFound).send({ detail : 'Error, server not found.' });
                return;
            }

            //  Si el código de respuesta que nos dio el microservicio es diferente de 200
            //  significa que el usuario que ingresó no se encontró en la base de datos, por lo que
            //  se deberá de informar al usuario de ello.
            if(response.statusCode != 200){
                console.error(response.body);
                resp.status(global.configuracion.estatusError.noAutorizado).send(response.body);
                return;
            }

            //  En caso de que el usuario haya ingresado usuario y contraseña correctamente, se deberá de
            //  almacenar la información de usuario (name, lastname, rol), así como el token que se generó
            //  al iniciar sesión.
            sesion.name = response.body.name;
            sesion.lastname = response.body.lastname;
            sesion.rol = response.body.rol;
            sesion.token = response.body.token;
            
            //  Se valida el valor del campo idmesa, que vendrá como null en caso de que el rol de usuario
            //  sea mesero. Si es mesa, el valor se tomará del body y se almacenará en las variables de sesión
            if (response.body.idmesa != null) {
                //  En caso de que sea diferente de null, se almacena y se guarda.
                sesion.idmesa = response.body.idmesa;   
            }

            //  Eliminamos del objeto recibido, información que no es necesaria para el flujo de la aplicación
            delete response.body.idusuario;
            delete response.body.sucursal;

            //  Se deberá de validar el rol que tiene asignado el usuario, para ver a que pantalla será
            //  redireccionado
            response.body.pantalla = validaDireccionamiento(response.body.rol);

            //  Se envia respuesta a frontend de la petición.
            resp.status(global.configuracion.estatusError.correcto).send(response.body);

        });

    }

});

/* Petición POST para cerrar sesión de usuario */
router.post('/salir', function(req, resp){
    //  Maneja cierre de sesión de la aplicación
    console.log('Llegó a salir.... routes/mesas.js');
    sesion = req.session;
    if(sesion.token){

        sesion.destroy(function(error){
            if(error){
               //   Existió un error al cerrar sesión, se debe de informar al usuario
                resp.status(global.configuracion.estatusError.badRequest).send({ detail : 'Error, in logout' });
            } else {
               //   En caso de que no exista ningún error cuando se eliminó la sesión, se debe de redirigir
               //   a la pantalla de iniciar sesión
                var redireccionar = {
                    pantalla : global.configuracion.listaRutas.login
                };
                resp.status(global.configuracion.estatusError.correcto).send(redireccionar);
            }

        });

    } else {
        //  La sesión que está activa, no tiene el token de usuario, por lo que se debe de
        //  redireccionar al usuario al apartado de iniciar sesión.
        console.error('No tiene token... no se puede cerrar sesión....');
        var redireccionar = {
            pantalla : global.configuracion.listaRutas.login,
            detail : 'User not authorized'
        };
        resp.status(global.configuracion.estatusError.noAutorizado).send(redireccionar);
    }
});

/* Petición POST para validar el regreso de la pantalla pagar, a que pantalla lo debe de redireccionar */
router.post('/validaRegresoPagar', function(req, resp){
    //  Se toma el valor de la sesión activa.
    sesion = req.session;
    if(sesion.token){
        //  Se debe de validar que tipo de usuario es, para
        //  identificar a que pantalla será redireccionado
        if(sesion.rol == global.configuracion.rolBuscar){
            //  Es mesero, debe de volver a la lista de mesas...
            resp.status(200).send({ pantalla :  global.configuracion.listaRutas.mesas});
        } else {
            //  Es mesa, debe de volver a la de atención...
            resp.status(200).send({ pantalla :  global.configuracion.listaRutas.atencion});
        }
        
    } else {
        //  En caso de que no exista el token en la variable de sesión, se manda a la pantalla de login.
        res.redirect(global.configuracion.listaRutas.login);
    }
    
});

//  Función que valida el rol de usuario
//  Se retorna la pantalla a la cual se deberá de redireccionar, dependiendo del rol que tenga.
function validaDireccionamiento (rol){

    if (rol == global.configuracion.rolBuscar){
        //  Si el rol que se envió es Mesero, se debe de redireccionar a la pantalla de mesas.
        return global.configuracion.listaRutas.mesas;

    } else {
        //  Si el rol es mesa, se redireccionará a la pantalla de tiempo.
        return global.configuracion.listaRutas.tiempo;
    }

}

//  Se expone el módulo de login del sistema
module.exports = router;
