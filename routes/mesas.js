var express = require('express');
var router = express.Router();
var sesion;
var request = require('request');
var config = require('../config.json');

/* Mapa de mesas */
router.get('/', function(req, res, next) {
    sesion = req.session;

    if(sesion.token){
        console.log('Si tiene token .... se debe validar rol');
        //  Se debe de validar que su rol en verdad sea el de mesero, para que pueda acceder a esta pantalla
        //  en caso de que sea mesa, se debe de redireccionar a la pantalla de tiempos.
        
        //   Si el token existe en la sesión de usuario, deberá de mandar a la pantalla según su usuario
        var pantalla = validaDireccionamiento(sesion.rol);
        if(pantalla){
            console.log('Si es mesero, por lo tanto se solicitan mesas....');
            //  Si el valor que devuelve es verdadero, quiere decir que si tiene acceso a esa pantalla
            //  Por lo que se debe de realizar la petición a microservicio y averiguar las mesas que 
            //  el mesero tiene asignadas.
            
            request("http://"+config.servidorMesas.direccionIP+":"+config.servidorMesas.puerto+config.servidorMesas.listaEndpoints.mesas + '?token=' + sesion.token, function(error, response, body){

                if(error){

                    res.status(500).render('mesas', {
                        title: 'Mesas asignadas',
                        name: sesion.name,
                        lastname: sesion.lastname,
                        mesa: 'error',
                        msjError: "Error, al obtener la información. Intentalo de nuevo",
                        fecha : fecha()
                    });
                    return;
                    
                } else{

                    mesas = JSON.parse(body); 
            
                    res.render('mesas', { 
                        title: 'Mesas asignadas',
                        name : sesion.name, 
                        lastname : sesion.lastname,
                        mesa: mesas, 
                        fecha : fecha() });

                }


            });  
            
        } else {
            //  En caso de que el valor sea falso, nos indica que el usuario en cuestión no es
            //  mesero, por lo que no debería de tener acceso a esta pantalla. Se debe de redireccionar
            //  a la pantalla que le corresponde.
            console.log('No es mesero, no tiene acceso a esta pantalla, se debe de redireccionar');
            res.redirect(global.configuracion.listaRutas.tiempo);
        }
        
    
    } else {
        console.log('No tiene sesión activa, se redirecciona a login....');
        res.redirect(global.configuracion.listaRutas.login);
    }

   
});

/* Endpoint al cual le llega la información del idmesa que el mesero ha seleccionado. Se almacena en las variables de
*  sesión y se redirige el usuario a la pantalla de tiempo. */
router.post('/direccionaTiempo', function(req, res, next){
    sesion = req.session;
    
    //  Se toma el valor de la mesa y se almacena en la variable de sesión.
    sesion.idmesa = req.body.idmesa;
    
    //  Se envia respuesta a front, con la pantalla que se va a renderizar.
    res.send({ redirect: global.configuracion.listaRutas.tiempo });
    
});

/** Endpoint al cual le llega la información de la idmesa que el mesero ha seleccionado. Se almacena en la
 *  variable de sesión y se redirige al usuario a la pantalla de pagar, con el id de mesa que le corresponde
 */
router.post('/direccionaPago', function (req, res, next) {
    sesion = req.session;
    
    //  Se toma el valor del id de pago seleccionado y se almacena en la variable de la mesa selecccionada
    sesion.idmesa = req.body.idmesa;
    
    //  Se envia respuesta a front con la pantalla que se va a renderizar.
    res.send({ redirect: global.configuracion.listaRutas.pagar });
});

//  Función que valida el rol de usuario
//  Se retorna true/false dependiendo si el usuario tiene acceso a esta pantalla.
function validaDireccionamiento (rol){

    if (rol == global.configuracion.rolBuscar){
        //  Si el rol que se envió es Mesero, se debe de redireccionar a la pantalla de mesas.
        return true;

    } else {
        //  Si el rol es mesa, se redireccionará a la pantalla de tiempo.
        return false;
    }

}

//  Función que retorna la fecha del sistema
function fecha() {

    var d = new Date();
    var dia = d.getDate();
    var arrMeses = new Array();
    arrMeses[0] = "Enero";
    arrMeses[1] = "Febrero";
    arrMeses[2] = "Marzo";
    arrMeses[3] = "Abril";
    arrMeses[4] = "Mayo";
    arrMeses[5] = "Junio";
    arrMeses[6] = "Julio";
    arrMeses[7] = "Agosto";
    arrMeses[8] = "Septiembre";
    arrMeses[9] = "Octubre";
    arrMeses[10] = "Noviembre";
    arrMeses[11] = "Diciembre";
    var mes = arrMeses[d.getMonth()];
    var ano = d.getFullYear();
    var fechaActual = dia + ' de ' + mes + ' de ' + ano;
    return fechaActual;
}

module.exports = router;

