var express = require('express');
var router = express.Router();
var request = require('request');
// Almacena la sesión del usuario
var sesion;

/* Pantalla para pago realizado */
router.get('/', function(req, res, next) {
  sesion = req.session;
/*  // DEMO FUNCIONAL
  var datos = {
    idmesa:1,
    idcomanda:2456,
    fecha:"27/04/2017",
    estatus:1,
    platillos:[
      {
        idplatillo:1,
        nombre:"Crema de elote con finas hierbas",
        cantidad:3,
        nota:"",
        precio:"75.5",
        tiempo:"10 min",
        subtotal:225
      },
      {
        idplatillo:2,
        nombre:"Aguacate relleno de chapulines",
        cantidad:2,
        nota:"Con salsa picosa",
        precio:"45",
        tiempo:"12 min",
        subtotal: 90
      },
      {
        idplatillo:3,
        nombre:"Michelada",
        cantidad:3,
        nota:"Victoria sin clamato",
        precio:"55",
        tiempo:"11 min",
        subtotal: 165
      },
      {
        idplatillo:4,
        nombre:"Tequila bandera mexicana",
        cantidad:1,
        nota:"",
        precio:"75",
        tiempo:"13 min",
        subtotal: 75
      }
    ]
  };

  validaTok (sesion, 1, function(rsl){
    if(rsl == 200){
      res.render('pagar', { title: 'Pedido realizado',
                            name: sesion.name,
                            lastname: sesion.lastname,
                            comanda : datos, fecha : fecha() });
    }
    else {
      console.log('Resultado de validacion ' + rsl);
      res.redirect('/');
    }
  });
*/
  var idMesa = req.session.idmesa;
  //**************************************
  var dataComanda = 'http://' + global.configuracion.servidorComandas.direccionIP + ':' + global.configuracion.servidorComandas.puerto + global.configuracion.servidorComandas.listaEndpoints.ordenActiva + '?idmesa=' + idMesa + '&token=' + sesion.token;
  console.log(dataComanda);
  // Petición
  request(dataComanda, function(err, response, body){
    if(err){
      console.error('Hubo error al realizar la petición de inicio de sesión.');
      //  En caso de que haya existido un error en la petición, se informa a usuario
      var pantalla = validaDireccionamiento (sesion.rol);
      res.redirect(pantalla);
      // res.status(404).send({ detail : 'Error, server not found.' });
      return;
    }
    if(response.statusCode == 200){
      var datosY = JSON.parse(response.body);
      validaTok (sesion, 1, function(rsl){
        if(rsl == 200){
          res.render('pagar', { title: 'Pedido realizado',
          name: sesion.name,
          lastname: sesion.lastname,
          comanda : datosY, fecha : fecha()});
        }
        else {
          console.log('Resultado de validacion ' + rsl);
          res.redirect('/');
        }
      }); // validaTok
    }
    else {
      console.log(response.statusCode + ' ' + body);
    }
  }); //request
  // Petición de comanda para dibujarla

});

/* Evento para invocar el microservicio de generarTicket */
router.post('/ventas', function(req, resp){
  // Asignamos el token al JSON del REQ
  req.body.token = req.session.token;
  req.body.idcomanda = sesion.idcomanda;
  // Datos de petición a microservicio
  var datPut = {
    url : 'http://' + global.configuracion.servidorVentas.direccionIP + ':' + global.configuracion.servidorVentas.puerto + global.configuracion.servidorVentas.listaEndpoints.venta,
    method : 'POST',
    json : true,
    body : req.body
  };
  // Petición
  request(datPut, function(err, response, body){
    if(err){
      console.error('Hubo error al realizar la petición de inicio de sesión.');
      //  En caso de que haya existido un error en la petición, se informa a usuario
      resp.status(404).send({ detail : 'Error, server not found.' });
      return;
    }
    if(response.statusCode !== 200){  // 201 Created
      console.log('Created');
      // Ha sido creado el ticket, a donde enviaremos ahora a la pantalla?
      if(sesion.rol == global.configuracion.rolBuscar)  // Mesero
      {
        resp.send({redirect :global.configuracion.listaRutas.mesas});
      }else {  // MESA
        // Cerrar sesión de la mesa
        validaTok (sesion, 2, function(rsl){
          if(rsl == 200){
            console.log('Se cerró sesión exitosamente');
            resp.send({redirect : '/salir'});
          }
          else{
            console.log('Error-----> ' + rsl);
            resp.send({redirect : '/salir'});
          }
        }); //validaTok
      }
    }
    else {
      resp.status(response.statusCode).send(response.body);
    }
  }); //request
}); //post /Ventas

/* Petición POST para validar el regreso de la pantalla pagar, a que pantalla lo debe de redireccionar */
router.post('/salirPagar', function(req, resp){
    //  Se toma el valor de la sesión activa.
    sesion = req.session;
    if(sesion.token){
        //  Se debe de validar que tipo de usuario es, para
        //  identificar a que pantalla será redireccionado
        if(sesion.rol == global.configuracion.rolBuscar){
            //  Es mesero, debe de volver a la lista de mesas...
            resp.status(200).send({ pantalla :  global.configuracion.listaRutas.mesas});
        } else {
            //  Es mesa, debe de volver a la de atención...
            resp.status(200).send({ pantalla :  global.configuracion.listaRutas.tiempo});
        }

    } else {
        //  En caso de que no exista el token en la variable de sesión, se manda a la pantalla de login.
        res.redirect(global.configuracion.listaRutas.login);
    }

});

//  Función que valida el rol de usuario
//  Se retorna la pantalla a la cual se deberá de redireccionar, dependiendo del rol que tenga.
function validaDireccionamiento (rol)
{
  if (rol == global.configuracion.rolBuscar){
    //  Si el rol que se envió es Mesero, se debe de redireccionar a la pantalla de mesas.
    return global.configuracion.listaRutas.mesas;
  } else {
    // delete sesion.idcomanda;
    //  Si el rol es mesa, se redireccionará a la pantalla de tiempo.
    return global.configuracion.listaRutas.tiempo;
  }
}

/****************************************************************************************************
* Valida toquen revisa que exista o si existe sesion
@param userSesion contiene los datos de la sesión del usuario
@param opcion   1 = Validación de token / 2 = Cierre de sesión
@param callback opcion de respuesta a quien invoca
Opción token 200 = Existe token   / 0 = Redirecciona a login
Opción Sesión  200 = sesión cerrada   / -1 = Error de sesión  / 0 = No existe redirecciona a login
****************************************************************************************************/
function validaTok (userSesion, opcion, callback)
{
  switch (opcion) {
    case 1:   // Validar token
    if(userSesion.token !== undefined){
      callback (200);
    }else {
      //  Si no, pinta la pantalla de login, para que se haga el proceso.
      delete sesion;
      callback (0);
    }
    break;

    case 2:   // Cerrar Sesion
    if(userSesion.token){
      userSesion.destroy(function(error){
        if(error){
          callback(-1);
        }
        else {
          delete sesion;
          callback(200);
        }
      });
    }
    else {
      callback (0);
    }
    break;
    default:
  }
}

//  Función que retorna la fecha del sistema
function fecha() {

    var d = new Date();
    var dia = d.getDate();
    var arrMeses = new Array();
    arrMeses[0] = "Enero";
    arrMeses[1] = "Febrero";
    arrMeses[2] = "Marzo";
    arrMeses[3] = "Abril";
    arrMeses[4] = "Mayo";
    arrMeses[5] = "Junio";
    arrMeses[6] = "Julio";
    arrMeses[7] = "Agosto";
    arrMeses[8] = "Septiembre";
    arrMeses[9] = "Octubre";
    arrMeses[10] = "Noviembre";
    arrMeses[11] = "Diciembre";
    var mes = arrMeses[d.getMonth()];
    var ano = d.getFullYear();
    var fechaActual = dia + ' de ' + mes + ' de ' + ano;
    return fechaActual;
}

module.exports = router;
