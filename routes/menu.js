var express = require('express');
var router = express.Router();
var request = require('request');
var sesion; //  Variable que gestiona los datos de la sesión activa
var config = require('../config.json');
var puerto = config.servidorComandas.puerto;

/* Menu y orden para mesa */
router.get('/', function(req, res, next) {

    sesion = req.session;

    /*var menuTotal = [
      {
        "idcategoria":4,
        "name":"Entradas",
        "platillos":[
          {
            "idplatillo":6,
            "nombre":"Taquitos vegetarianos",
            "precio":"75.00",
            "imagen":"https://www.philadelphia.com.mx/modx/assets/img/revision2016/images/recetas/tacos_frito_vegetariano_194221_F_B01.jpg",
            "descripcion":"Taquitos dorados, rellenos de zanahoria, papa y calaza rallada, servidor con verduras.",
            "tiempo":"18 min"
          },
          {
            "idplatillo":7,
            "nombre":"Papitas cambray",
            "precio":"65.00",
            "imagen":"https://static.betazeta.com/www.sabrosia.com/up/2014/05/papas-960x623.jpg",
            "descripcion":"Tiernas y suaves papitas de cambray, traidas desde las tierras míticas de Narnia.",
            "tiempo":"15 min"
          }
        ]
      },
      {
        "idcategoria":5,
        "name":"Ensaladas",
        "platillos":[

        ]
      },
      {
        "idcategoria":6,
        "name":"Sopas y Pastas",
        "platillos":[

        ]
      },
      {
        "idcategoria":7,
        "name":"Pollo y Cortes",
        "platillos":[

        ]
      },
      {
        "idcategoria":8,
        "name":"Pescados y Mariscos",
        "platillos":[

        ]
      },
      {
        "idcategoria":9,
        "name":"Antojitos Mexicanos",
        "platillos":[

        ]
      },
      {
        "idcategoria":10,
        "name":"Bebidas",
        "platillos":[

        ]
      }
    ];*/
    
    if (sesion.token){
        
        var urlCategorias = "http://" + config.servidorPlatillos.direccionIP + ":"+ config.servidorPlatillos.puerto + config.servidorPlatillos.listaEndPoints.platillos + '?idgrupo=' + sesion.idgrupo + '&token=' + sesion.token;
        console.log(urlCategorias);
        request(urlCategorias, function(reqPlat, resPlat, bodyPlat){
            if (reqPlat){
              console.error('Hubo error.... :S al solicitar platillos');
              return res.status(500).send(request);
            }
            if(resPlat.statusCode==200){
                // Tenemos los platillos
                console.log('Mis platillos=');
                console.log(resPlat.body);
                  //datos para consultar la comanda Activa
                console.log('Iniciamos la peticion al microservicio');
                  var idmesa = sesion.idmesa;
                  var token = sesion.token;

                // Petición Request de la Comanda Activa
                request("http://" + config.servidorComandas.direccionIP + ":"+ config.servidorComandas.puerto + config.servidorComandas.listaEndpoints.ordenActiva + "?idmesa=" + idmesa + "&token=" + token,function(request, response, body){

                    if (request){
                      console.error('Hubo error.... :S :S al solicitar la comanda activa....');
                      return res.status(500).send(request);
                      //    Implementar manejo de error en front... 
                    }

                    if(response.statusCode==200){
                      console.log("Correcto");
                      body = JSON.parse(body);
                        console.error(resPlat.body);
                        var menuTotal = JSON.parse(resPlat.body);

                        res.render('menu', {
                          title: 'Nuestro Menú-Tiempo ' + sesion.nombreTiempo,
                          fecha :  fecha(),
                          ComandaVacia : "llena",
                          tiempo : sesion.nombreTiempo,
                          comandaActiva : body,
                          menu : menuTotal,
                          name : sesion.name,
                          lastname : sesion.lastname
                        });
                    }
                    else {
                      console.log("Error!!222");

                        console.error(resPlat.body);
                        
                        var menuTotal = JSON.parse(resPlat.body);

                      var ComandaVacia = "vacia";
                      res.render('menu', {
                          title: 'Nuestro Menú-Tiempo ' + sesion.nombreTiempo,
                          fecha :  fecha(),
                          ComandaVacia : "vacia",
                          comandaActiva : ComandaVacia,
                          menu : menuTotal,
                          tiempo : sesion.nombreTiempo,
                          name : sesion.name,
                          lastname : sesion.lastname
                      });
                    }
                  });//REQUEST de comanda activa

            }
            else {
                var menu = {
                    idcategoria : 3,
                    nombre : 'Entradas',
                    platillos : []
                };
                      console.log("Error!! 33");
                      var ComandaVacia = "vacia";
                      res.render('menu', {
                          title: 'Nuestro Menú-Tiempo ' + sesion.nombreTiempo,
                          fecha :  fecha(),
                          ComandaVacia : "vacia",
                          comandaActiva : ComandaVacia,
                          menu : menu,
                          tiempo : sesion.nombreTiempo,
                          name : sesion.name,
                          lastname : sesion.lastname
                      });
                    }
        }); // REquest de Platillos
        
    } else {
        res.redirect(global.configuracion.listaRutas.login);
    }
    
    
});//get"/"

router.post('/realizaComanda', function(req, res, next) {

  console.log("llega a realizaComanda"+req);
    console.log(req);
  sesion = req.session;
    console.error(sesion);

//variable que lleva el token generado cuando se inicia la session
  var token = sesion.token;

//Esqueleto de la peticion
  var datosPeticion = {
            url : "http://" + config.servidorComandas.direccionIP + ":"+ config.servidorComandas.puerto + config.servidorComandas.listaEndpoints.realizarOrden + '?token='+token, //'http://' + global.configuracion.servidorAutenticacion.direccionIP + ':' + global.configuracion.servidorAutenticacion.puerto + global.configuracion.servidorAutenticacion.listaEndpoints.login,
           method : 'POST',
            json : true,
            body : req.body
        }

  request(datosPeticion,function(err,response,body){

    console.log(body);

    sesion.idcomanda = body.idcomanda
    //si la peticion no se realiza
  /*  if (err) {
      console.log('Hubo un error al realizar la peticion.');
      //enviamos la respuesta al usuario
    }
    if(response.statusCode != 200){
      console.log(response.body);

    }*/
    res.status(200).send(response.body);

  });

  });

//  Función que retorna la fecha del sistema
function fecha(){

    var d = new Date();
    var dia = d.getDate();
    var arrMeses = new Array();
    arrMeses[0] = "Enero";
    arrMeses[1] = "Febrero";
    arrMeses[2] = "Marzo";
    arrMeses[3] = "Abril";
    arrMeses[4] = "Mayo";
    arrMeses[5] = "Junio";
    arrMeses[6] = "Julio";
    arrMeses[7] = "Agosto";
    arrMeses[8] = "Septiembre";
    arrMeses[9] = "Octubre";
    arrMeses[10] = "Noviembre";
    arrMeses[11] = "Diciembre";
    var mes = arrMeses[d.getMonth()];
    var ano = d.getFullYear();
    var fechaActual = dia + ' de ' + mes + ' de ' + ano;
    return fechaActual;
};


module.exports = router;
